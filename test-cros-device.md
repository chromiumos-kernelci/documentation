## Running KernelCI tests on a ChromiumOS device

Running tests on ChromiumOS devices is an essential part of this project.
ChromiumOS even provides a full test suite, known as "Tast tests", which are
actually used as KernelCI test plans for those devices.

### Test (plan) definitions

The test definitions include both the definitions of tests themselves, but also
the list of tests to be executed on a specific device type. This information
can be found in the [kernelci-core](https://github.com/kernelci/kernelci-core/)
repository, inside the `test-configs-chromeos.yaml` file. This file sits under
the `config/core` subfolder.

There are actually two sections of this file which are used for configuring
which tests should be executed for a given device type:
* the definitions for the tests themselves, located under the `test_plans`
  section
* the list of test plans to be executed for a given device, located under the
  `test_configs` section

#### Test definitions

Each test plan is executed as a separate LAVA job. Therefore, test plan
definitions are used primarily for generating the LAVA jobs running the needed
test(s) on the chosen target device(s). Those definitions are written using the
following format:

```yaml
  <plan_name>:
    pattern: '<job template>'
    filters: # optional
      - blocklist: { defconfig: ['forbidden_defconfig'] }
      - passlist: { defconfig: ['allowed_defconfig'] }
    params:
      job_timeout: '<timeout in minutes>'
      docker_image: '<docker image name>'
      fixed_kernel: <boolean> # optional
      <additional job parameters> # optional
```

`pattern` defines the jinja2 template to be used for generating the test job.
It must contain the path to the template, relative to the `config/lava`
subfolder of `kernelci-core`. For example,
`pattern: 'chromeos/cros-boot.jinja2'` means the job should be generated using
the `<kernelci-core>/config/lava/chromeos/cros-boot.jinja2` template file.

The `filters` section of test definitions isn't really relevant to ChromeOS
testing.

The parameters under `params` are substituted to corresponding template
variables when generating the LAVA job definition. Among those, `fixed_kernel`
is especially important: by default, jobs will run using a specific kernel
defined through the `kernel_url`, `modules_url` and optional `initrd_url` and
`dtb_url` template variables.

Those parameters can contain any **publicly-accessible** URL, which will then
be used by LAVA to fetch the corresponding artifacts. However, when
`fixed_kernel` is `true`, the *reference* kernel (the one defined under the
`reference_kernel` section of the
[device type definition](flash-cros-device.md#device-definitions-and-required-files))
will be used.

Running 2 otherwise identical jobs, differing only in the value of
`fixed_kernel`, is therefore a simple way to detect regressions between the
reference kernel and the test kernel.

##### Tast tests

[Tast](https://chromium.googlesource.com/chromiumos/platform/tast/) is a test
suite dedicated to ChromeOS and used as our primary means of testing ChromeOS
devices. Tast-based test plans are defined as follows:

```yaml
  cros-tast-base: &cros-tast-base
    pattern: 'chromeos/cros-tast.jinja2'
    params: &cros-tast-base-params
      job_timeout: '60'
      docker_image: 'kernelci/cros-tast'

  cros-tast-hardware: &cros-tast-hardware
    <<: *cros-tast-base
    params: &cros-tast-hardware-params
      <<: *cros-tast-base-params
      tests: >
        hardware.DiskErrors
        hardware.SensorAccel
        hardware.SensorIioservice
        hardware.SensorIioserviceHard
        hardware.SensorLight
        hardware.SensorPresence
        hardware.SensorActivity
        graphics.HardwareProbe
        health.ProbeSensorInfo
        health.DiagnosticsRun.*
        health.ProbeAudioHardwareInfo
        health.ProbeAudioInfo
        health.ProbeBacklightInfo
        health.ProbeCPUInfo
        health.ProbeFanInfo
        inputs.PhysicalKeyboardKernelMode
        graphics.KernelConfig
        graphics.KernelMemory

  cros-tast-hardware-fixed:
    <<: *cros-tast-hardware
    params:
      <<: *cros-tast-hardware-params
      fixed_kernel: true
```

Each Tast test plan is derived from `cros-tast-base` which sets the common
parameters: job template, timeout and Docker image to be used (as well as the
filters, not shown in the above example for clarity).

Specific test plans then only needs to define the list of Tast tests to be
executed, and optionally override the `fixed_kernel` parameter. As a matter of
fact, each `cros-tast-<test>` test plan is supplemented by a corresponding
`cros-tast-<test>-fixed` plan in order to easily detect regressions is test
kernels.

#### Per-device test configurations

Those configurations are simply lists of tests to be run on each device type.

```yaml
  - device_type: <device_type>
    filters: # optional, same format as test plans
    test_plans:
      - test-plan-1
      - test-plan-2
      - test-plan-3
      - ...
```

Where `test-plan-*` have matching entries in the `test_plans` section.

There can be multiple entries for a single `device_type`, each entry containing
a different list of tests and/or filters.

*Note: currently, only the first entry for a given device type is taken into
account by `cros-tester`.*

### Setting up the work directory

Unless done already, a work directory must be setup according to
[those instructions](README.md#setting-up-a-work-directory).

### Running test jobs

A testing job is created and submitted by running the following command:
```sh
  $ <path/to/kci-tools>/cros-testing/cros-tester.py --test <device>
```

Where `<device>` can be one of:
* the full identifier for one specific device present in the lab
  (e.g. `mt8192-asurada-spherion-r0-cbg-0`)
* the device type name (e.g. `mt8192-asurada-spherion-r0`); in this case, all
  devices of this type will be tested

Specific test plans can be selected using the `--filter` command-line option,
followed by the comma-separated list of tests to execute.

```sh
  $ <path/to/kci-tools>/cros-testing/cros-tester.py --test <device> --filter cros-baseline,cros-tast-hardware,cros-tast-sound
```

In the above example, `cros-tester` will create jobs corresponding to those 3
tests: `cros-baseline`, `cros-tast-hardware` and `cros-tast-sound`.

`--filter` also accepts 3 special values:
* `all`: run all tests defined in the test config of the chosen device type
* `fixed`: run all test plans ending with `-fixed` for the chosen device type
* `notfixed`: run all test plans *not* ending with `-fixed` for the chosen
  device type

Those special values can be useful for generating a reference set of results
using `--filter fixed`, then testing multiple kernels with `--filter notfixed`
on each run.

*Note: when `--filter` is omitted, only the first test plan found in the test
config corresponding to the selected device will be executed. Usually, this
will be `cros-baseline`.*

Finally, the test kernel (if needed, i.e for all non-`fixed` jobs) can be
specified using the `--kernel` command-line option. Based on the device type
definition, it will then automatically construct the corresponding URLs for
both the module tarball and DTB (when the latter is required).

```sh
  $ <path/to/kci-tools>/cros-testing/cros-tester.py --test <device> --kernel https://example.com/path/to/kernel/Image
```

*Note: `cros-tester` expects the modules tarball to be under the same folder as
the kernel image, and the DTBs to be under a subfolder named `dtbs`. In the
above example, that means the following URLs will be used:*
* *`https://example.com/path/to/kernel/Image`*
* *`https://example.com/path/to/kernel/modules.tar.xz`*
* *`https://example.com/path/to/kernel/dtbs/<dtb>`, with `<dtb>` being the value
  of the toplevel `dtb` property in the device type definition*

### Test job results

`cros-tester` prints the submitted job number, name and URL on the console for
each job, and waits for the job to complete before printing the results:
* `Job OK` if the job succeeds
* `Job failed` along with the log lines showing which tests or sub-tests failed

It also creates 2 subfolders under the work directory:
* `reports` contain the job report in JSON format, including the job ID, the
  device it ran on, its definition, state and full log
* `results` contain Tast test results in CSV format, both per job
  (`<job_id>_<test_plan>_<device_type>.csv`) and per run of `cros-tester`
  (`<job_id>_<plan_name>_<device_type>.csv`); those files can be easily
  copy/pasted in spreadsheets/reports such as
  [this one](https://docs.google.com/spreadsheets/d/1YWPRG_bu359aJjnKgCYY1N0DsPyZgvrXfrQvU3GiI1c/edit?usp=sharing)
