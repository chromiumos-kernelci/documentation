## Flashing a ChromiumOS device from KernelCI

Devices in the LAVA lab can't be flashed manually, and therefore require a
specific procedure to be executed, using LAVA jobs and based on the device
definitions used by KernelCI.

This can be executed both for upgrading to a newer ChromiumOS release or for
flashing a custom-built image in anticipation of a testing "campaign".

**Note: when flashing a custom image to a device in the LAVA lab, please ensure
the device stays in that state for a very limited period of time (whenever
possible, this period should not exceed the duration of the tests) and it is
put back in its "normal" state (i.e. using the reference ChromiumOS build)
afterwards.**

### Device definitions and required files

The device definitions, and more specifically the locations of the rootfs and
kernel files corresponding to the current reference build, can be found in the
[kernelci-core](https://github.com/kernelci/kernelci-core/) repository, inside
the `test-configs-chromeos.yaml` file. This file sits under the `config/core`
subfolder. The part relevant to this document is the `device_types` section,
containing one entry per device with the following information:

```yaml
  <device_type>_chromeos:
    base_name: <device_type>
    mach: <soc_vendor>
    class: arm64-dtb
    boot_method: depthcharge
    dtb: '<name of the DTB file, including the vendor folder>'
    filters: [blocklist: {}]
    params:
      cros_flash_nfs:
        base_url: '<base URL of the system used for flashing the device>'
        initrd: 'initrd.cpio.gz'
        initrd_compression: 'gz'
        rootfs: 'full.rootfs.tar.xz'
        rootfs_compression: 'xz'
      cros_flash_kernel:
        base_url: '<base URL of the kernel used for flashing the device>'
        image: 'kernel/Image'
        modules: 'modules.tar.xz'
        modules_compression: 'xz'
        dtb: '<path of the DTB file relative to the above base URL>'
      cros_image:
        base_url: '<base URL of the ChromiumOS image files>'
        flash_tarball: 'full-cros-flash.tar.gz'
        flash_tarball_compression: 'gz'
        tast_tarball: 'tast.tgz'
      reference_kernel:
        image: '<URL of the Image file for the device>'
        modules: '<URL of the modules.tar.xz file for the device>'
        dtb: '<URL of the DTB file for the device>'
      block_device: detect
```

The above example relates to ARM64 devices, for x86-based ChromeBooks configs
are usually a bit simpler:
* no DTB-related parameter is needed
* `mach` is always `x86` (except for `qemu` targets, which are currently
  disabled)
* `class` is replaced by `arch: x86_64`; the former is actually a shortcut for
  `arch: arm64` and implies the need of the `dtb` attribute

The files mentioned under the `cros_flash_nfs` section are usually not specific
to any device, but instead are shared among all devices sharing the same
architecture. For `cros_flash_kernel`, the current state is the following:
* x86 devices: a single kernel is used for all devices, so this section is
  inherited from a reference entry
* ARM64 devices: the kernel is usually specific to the SoC vendor (but not tied
  to a single device type), and each device uses a specific DTB; this requires
  adding a `cros_flash_kernel` entry to all device definitions for this
  architecture, although only the `dtb` parameter will differ, and the
  `base_url` depending on the SoC vendor

The other files are device-specific, though.

*Note: all URLs must be **publicly** accessible over HTTPS*

### Setting up the work directory

Unless done already, a work directory must be setup according to
[those instructions](README.md#setting-up-a-work-directory).

The flashing script, however, requires an additional step to be executed: the
[templates](https://gitlab.collabora.com/chromiumos-kernelci/kci-tools/-/tree/main/cros-flashing/templates)
folder of the `cros-flasher` tool must be copied to the work directory (or
linked from your local working copy of the repo). This step is needed as the
script doesn't use job templates from `kernelci-core` at the moment, and
instead relies on its own (simpler) job templates.

### Flashing the reference image

For flashing reference images, the local copy of `kernelci-core`, or really its
`test-configs-chromeos.yaml` file, must be unmodified: this is the only way to
ensure the current reference image is *actually* used.

It is advised that a separate work directory is used, in which `kernelci-core`
is cloned and kept updated on a regular basis; this copy should be considered
read-only in order to avoid juggling with feature branches and/or hazardous
rebases and ensure a clean state of this repo.

A flashing job is then created and submitted by running the following command:
```sh
  $ <path/to/kci-tools>/cros-flashing/cros-flasher.py --flash <device>
```

*Note: this command also executes a basic test after successfully flashing the
device as a sanity check*

Where `<device>` can be one of:
* the full identifier for one specific device present in the lab
  (e.g. `mt8192-asurada-spherion-r0-cbg-0`)
* the device type name (e.g. `mt8192-asurada-spherion-r0`); in this case, all
  devices of this type will be flashed

In general, it is best to flash only one specific device when running tests
using a custom-built system, and reflash it with the reference image
afterwards.

Flashing all the devices of a specific type is however useful after upgrading
the reference image to a newer ChromiumOS release.

For more advanced usage, please refer to the
[cros-flasher documentation](https://gitlab.collabora.com/chromiumos-kernelci/kci-tools/-/blob/main/cros-flashing/cros-flasher.md).

### Flashing a custom-built image

As previously mentioned, all image and kernel files must be stored on a
publicly-accessible web server. This requires that the developer has access to
such a server and is able to upload the corresponding files there.

Once the files are uploaded, `test-configs-chromeos.yaml` must be modified,
changing the base URLs to match the location of the custom files. The following
parameters are affected:
* `cros_image`
  * `base_url`
* `reference_kernel`
  * `image`
  * `modules`
  * `dtb` (ARM64 devices only)

Once those changes have been implemented, the workflow is identical to flashing
a reference image.

*Note: remember to reflash the device with the reference image*
