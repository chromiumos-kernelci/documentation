## ChromeOS on KernelCI documentation

This repository is meant to be the reference documentation for the work
Collabora is doing for ChromeOS on KernelCI. It describes our tools, processes
and workflows.

### Tools

[kci-tools](https://gitlab.collabora.com/chromiumos-kernelci/kci-tools) is a
set of internal tools used to ease work on ChromiumOS in the context of
KernelCI.

### Workflows

* [Flash a CrOS image](test-cros-device.md) to a specific device
* [Run tests](test-cros-device.md) on a specific device

### Common procedures

#### Setting up a work directory

Flashing and testing a device from the LAVA lab can be done using scripts from
[kci-tools](https://gitlab.collabora.com/chromiumos-kernelci/kci-tools), which
share the same prerequisites:
* an authentication token for LAVA (stored in a plaintext file named
  `token.secret` at the root of the work directory)
* a copy of [kernelci-core](https://github.com/kernelci/kernelci-core/) from
  which to retrieve device definitions, test plans and other parameters

If at least one those elements is missing from the work directory, either
[cros-flasher](flash-cros-device.md) or [cros-tester](test-cros-device.md) must
be executed with the `--init` command-line argument.

This will force the script to prompt the user for an authentication token, then
clone `kernelci-core` inside the work directory, checking out the upstream
`chromeos.kernelci.org` branch. Finally, the script will use this data to query
the list of devices from LAVA, which can be printed by re-running the script
with the `--list` command-line option.

For more fine-grained control, it is recommended that users either clone the
`kernelci-core` repository themselves, potentially selecting a different remote
(such as a personal fork) or branch, or create a symlink pointing to a local
working copy of this repository.

*Note: the scripts from `kci-tools` only read data from `kernelci-core` and
never overwrite anything there.*
